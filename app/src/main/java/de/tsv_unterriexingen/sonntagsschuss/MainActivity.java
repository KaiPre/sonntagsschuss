package de.tsv_unterriexingen.sonntagsschuss;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView iv = (ImageView) findViewById(R.id.imageView_pdfContent);
        iv.setImageBitmap(renderToBitmap(getApplicationContext(), "Dok1.pdf"));

    }

    /**
     * Use this to load a pdf file from your assets and render it to a Bitmap.
     *
     * @param context
     *            current context.
     * @param filePath
     *            of the pdf file in the assets.
     * @return a bitmap.
     */
    @Nullable
    public static Bitmap renderToBitmap(Context context, String filePath) {
        Bitmap bi = null;
        InputStream inStream = null;
        try {
            AssetManager assetManager = context.getAssets();
            Log.d("TAG", "Attempting to copy this file: " + filePath);
            inStream = assetManager.open(filePath);
            bi = renderToBitmap(context, inStream);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inStream.close();
            } catch (IOException e) {
                // do nothing because the stream has already been closed
            }
        }
        return bi;
    }

    /**
     * Use this to render a pdf file given as InputStream to a Bitmap.
     *
     * @param context
     *            current context.
     * @param inStream
     *            the inputStream of the pdf file.
     * @return a bitmap.
     * @see https://github.com/jblough/Android-Pdf-Viewer-Library/
     */
    @Nullable
    public static Bitmap renderToBitmap(Context context, InputStream inStream) {
        Bitmap bi = null;
        try {
            byte[] decode = IOUtils.toByteArray(inStream);
            net.sf.andpdf.nio.ByteBuffer buf = net.sf.andpdf.nio.ByteBuffer.wrap(decode);
            PDFPage mPdfPage = new PDFFile(buf).getPage(0);
            float width = mPdfPage.getWidth();
            float height = mPdfPage.getHeight();
            RectF clip = null;
            bi = mPdfPage.getImage((int) (width), (int) (height), clip, true,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                inStream.close();
            } catch (IOException e) {
                // do nothing because the stream has already been closed
            }
        }
        return bi;
    }

}
